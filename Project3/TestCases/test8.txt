-- Relational and logical operators

function main returns boolean;
 begin

(5 + 3 > 8 or 9 / 3 = 4) or (7 - 9 > 3) and ( (3 /= 1 * 7) or 8 <= 7 and 3 >= 9);
 end;
