 -- Author: 	Ryan Bramich
 -- Class:  	CMSC 430 6381
 -- Assignment:	Project 2
 -- File:	SynErrEnd.txt
 -- Numb of errors in file: 0 - Lex, 1 - Syn 0 - Sem
 -- Error missing ';' after end token.

function test1 returns integer;
begin
    7 + 2 * (2  + 4);
end
